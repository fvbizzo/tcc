const Plataform = artifacts.require('./Plataform.sol')
const TestUtils = require('./TestUtils')

contract(Plataform, function(accounts) {

    let plataform;

    const utils = new TestUtils();

    beforeEach('setup contract for each test', async function (){
        plataform = await Plataform.new();
    })
    
    it ('should get the right pool id', async function() {
        await plataform.newPool(10, "pool1", 30, {from: accounts[0]});

        const _poolid = web3.toAscii(await plataform.getPoolId("pool1"));

        assert.equal(_poolid.valueOf(), "pool1", "pool1 wasnt the pool id.");
    })



})
