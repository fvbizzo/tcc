pragma solidity ^0.5.0;

contract Plataform {

    address owner;

    struct Pool {
        bytes32 poolId;
        uint minMarge;
        uint numTokens;
        address payable poolOwner;
        bool isPoolIdUsed;

    }

    struct Participant {
        address id;
        mapping (bytes32 => uint) tokens;
    }

    struct Auction {
        bytes32 poolId;
        uint value;
        uint amount;
        uint id;
    }

    mapping(bytes32 => Pool) internal pools;
    mapping(address => Participant) internal participants;
    mapping(uint => Auction) internal auctions;

    uint ownerFee = 2;
    uint totalFees = 0;
    uint auctionCounter = 0;

    constructor () public {
        owner = msg.sender;
    }

    modifier onlyOwner ()  {
        require (msg.sender == owner);
        _;
    }

    function newPool (uint MinMarge, bytes32 Id, uint numTokens) public {
        
        Pool storage pool = pools[Id];
        require(pool.isPoolIdUsed == false);

        pool.poolId = Id;
        pool.minMarge = MinMarge;
        pool.numTokens = numTokens;
        pool.isPoolIdUsed = true;
        pool.poolOwner = msg.sender;

    }

    function newAuction(uint amount, bytes32 poolid, uint price) public {
        Pool storage pool = pools[poolid];
        Participant storage participant = participants[msg.sender];
        require(pool.isPoolIdUsed == true);
        require(participant.tokens[poolid] >= amount);
        require(price >= pool.minMarge);
        Auction storage auction = auctions[auctionCounter];
        auction.id = auctionCounter;
        auction.poolId = poolid;
        auction.amount = amount;
        auction.value = price;


        auctionCounter += 1;
        
    }

    function transferTokens(uint amount, bytes32 poolid, address receiverAddress) public {
        Pool storage pool = pools[poolid];
        Participant storage giver = participants[msg.sender];
        Participant storage receiver = participants[receiverAddress];
        require(pool.isPoolIdUsed == true);
        require(giver.tokens[poolid] >= amount);
        giver.tokens[poolid]-=amount;
        receiver.tokens[poolid]+=amount;

    }

    function buyTokens(uint amount, bytes32 poolid) public payable {
        Pool storage pool = pools[poolid];
        Participant storage participant = participants[msg.sender];
        require(pool.isPoolIdUsed == true);
        require(amount <= pool.numTokens);
        uint totalAmount = amount*pool.minMarge;
        require(msg.value >= totalAmount);
        uint sellerAmount = (totalAmount)*(100-ownerFee)/100;

        pool.poolOwner.transfer(sellerAmount);
        participant.tokens[poolid] = amount;
        totalFees += totalAmount - sellerAmount; 

        msg.sender.transfer(msg.value - totalAmount);

    }

    function passOwnership(address newOwner) onlyOwner public {
        owner = newOwner;
    }

    function ownerWithdraw() onlyOwner public {
        msg.sender.transfer(totalFees);
    }

    function getPoolId(bytes32 poolid) public view returns (bytes32) {
        return pools[poolid].poolId;
    }

    function getUserTokens(bytes32 poolid) public view returns (uint) {
        return participants[msg.sender].tokens[poolid];
    }

}