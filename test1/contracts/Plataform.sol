pragma solidity ^0.5.0;

contract Plataform {

    address owner;

    struct Pool {                           //Struct usada para representar um fundo
        bytes32 poolId;                     //id do fundo
        uint minMarge;                      //valor mínimo de venda de um token
        uint numTokens;                     //quantidade total de tokens desse fundo
        address payable poolOwner;          //endereço do criador do fundo 
        bool isPoolIdUsed;                  //booleana utilizada para verificar se o id do fundo está disponível
    }

    struct Participant {                    //Struct usada para representar um participante em qualquer fundo
        address id;                         //endereço do participante usado como identificador
        mapping (bytes32 => uint) tokens;   //mapping para representar quantos tokens o participante tem em cada fundo
    }

    struct Auction {                        //Struct usada para representar um anuncio
        bytes32 poolId;                     //id do fundo ao qual os tokens sendo vendidos no anuncio pertencem
        uint value;                         //preço de venda dos tokens do anuncio 
        uint amount;                        //quantidade de tokens sendo oferecidos no anuncio 
        uint id;                            //id do anuncio
        address payable owner;              //endereço do participate que criou o anuncio
    }

    struct Voting {                         //Struct usada para representar uma votação
        bytes32 poolId;                     //id do fundo ao qual a votação pertence
        uint propositon;                    //novo valor proposto para ser o mínimo
        address proposer;                   //endereço de qual participante fez a proposta
        uint totalVotes;                    //número total de votos possíveis
        uint yayVotes;                      //número de votos favoráveis a mudança
        uint nayVotes;                      //número de votos contrários a mudança
        mapping(address => bool) hasVoted;  //mapping para registrar quais endereços já votaram
        bool isVotingHappening;             //booleana que verifica se já existe uma votação aberta
    }
    
    struct Propose {
        address proposer;
        address receiver;
        uint amount;
        bytes32 pool;
        uint proposeId;
    }

    mapping(bytes32 => Pool) internal pools;                //mapping para os fundos.
    mapping(address => Participant) internal participants;  //mapping para todos os participantes de todos os fundos.
    mapping(uint => Auction) internal auctions;             //mapping de todas as açoes de vendas.
    mapping(bytes32 => Voting) internal votings;            //mapping de todas as votações de diferentees fundos.
    mapping(uint => Propose) internal proposes;             //mapping de todas as propostas de transferência.

    uint ownerFee = 2;                                  
    uint totalFees = 0;
    uint auctionCounter = 0;
    uint proposeCounter = 0;

    constructor () public {
        owner = msg.sender;
    }

    modifier onlyOwner ()  {
        require (msg.sender == owner);
        _;
    }

    //funcao que cria um novo fundo, recebe a margem minima inicial, um id para o fundo, o total de tokens e os tokens que vão ser colocados a venda. Todos os tokens
    //colocados a venda são vendidos pelo preço inicial e todos os que não estiverem na venda inicial são atrelados ao dono do fundo como se fosse um participante normal.
    function newPool (uint MinMarge, bytes32 Id, uint totalTokens, uint tokensForSale) public {
        
        Pool storage pool = pools[Id];
        require(pool.isPoolIdUsed == false);
        Participant storage participant = participants[msg.sender];


        participant.tokens[Id] = totalTokens;
        pool.poolId = Id;
        pool.minMarge = MinMarge;
        pool.numTokens = totalTokens;
        pool.isPoolIdUsed = true;
        pool.poolOwner = msg.sender;

        newAuction(tokensForSale, Id, MinMarge);

    }

    //funcao para criar um novo anúncio, recebe a quantidade de tokens que serão anunciados, de qual pool eles pertencem e o preço de venda
    //verifica se a pessoa tem aqueles tokens, e também se o preço está acima da margem mínima.
    //cria um novo anúncio e desconta o número de tokens do anunciante.
    function newAuction(uint amount, bytes32 poolid, uint price) public {
        Pool storage pool = pools[poolid];
        Participant storage participant = participants[msg.sender];

        require(pool.isPoolIdUsed == true);
        require(participant.tokens[poolid] >= amount);
        require(price >= pool.minMarge);

        Auction storage auction = auctions[auctionCounter];
        auction.id = auctionCounter;
        auction.poolId = poolid;
        auction.amount = amount;
        auction.value = price;
        auction.owner = msg.sender;

        auctionCounter += 1;

        participant.tokens[poolid] -= amount;
    }

    //função que cancela um anúncio previamente feito por um integrante de uma pool.
    //recebe o id do anúncio e verifica se quem está chamando a função é o mesmo endereço que criou o anúncio.
    //retorna os tokens para o dono e deleta as informações do anúncio.
    function cancelAuction(uint auctionId) public {
        Auction storage auction = auctions[auctionId];
        require(auction.owner == msg.sender);

        Participant storage participant = participants[msg.sender];

        participant.tokens[auction.poolId] += auction.amount;

        delete auctions[auctionId];


    }

    //função que transfere tokens entre dois endereços sem transação monetária.
    //recebe a quantidade de tokens, o pool e o endereço do recebedor como parâmetros.
    //verifica se o endereço que chamou a função tem os tokens necessários para a transação e transfere para o novo endereço.
    function transferTokens(uint amount, bytes32 poolid, address receiverAddress, address giverAddress) public  {
        Pool storage pool = pools[poolid];
        Propose storage propose = proposes[proposeCounter];
        require(pool.isPoolIdUsed == true);
        require(giver.tokens[poolid] >= amount);
        
        propose[proposer] = giverAddress;
        propose[receiver] = receiverAddress;
        propose[amount] = amount;
        propose[poolId] = poolid;
        propose[proposeId] = proposeCounter;
        
        proposeCounter += 1;

    }
    
    function resolveTransfer(bool approval, uint transferid ) public onlyOwner {
        Propose storage propose = proposes[transferid];
        Participant storage giver = participants[giverAddress];
        Participant storage receiver = participants[receiverAddress];
        
        if(approval == true) {
            giver.tokens[poolid] -= amount;
            receiver.tokens[poolid] += amount;
            delete proposes[transferid];
        }
        else {
            delete proposes[transferid];
        }
    }

    //função que realiza a compra de um anúncio de tokens.
    //recebe a quantidade de tokens que será comprada e o id do anúncio, essa função é do tipo payable, logo também deve ser enviado ether ao ser chamada.
    //verifica se existem tokens suficientes para a transação, e se foi enviado uma quantidade suficiente de ether para a função.
    //transfere os tokens para o comprador, subtrai o total de tokens disponíveis no anúncio, transefere o dinheiro devido ao vendedor e a taxa é armazenada no contrato.
    //após todas as transações, a função devolve o troco para quem a chamou, se houver.
    function buyTokens(uint amount, uint auctionid) public payable {
        Auction storage auction = auctions[auctionid];
        Participant storage participant = participants[msg.sender];
        require(auction.amount > 0);
        require(auction.amount >= amount);
        uint totalAmount = amount*auction.value;
        require(msg.value >= totalAmount);
        uint sellerAmount = (totalAmount)*(100-ownerFee)/100;

        auction.owner.transfer(sellerAmount);

        participant.tokens[auction.poolId] += amount;
        auction.amount -= amount;
        totalFees += totalAmount - sellerAmount;

        msg.sender.transfer(msg.value - totalAmount);

    }

    function newVoting(bytes32 poolid, uint PropositionValue) public {
        Voting storage voting = votings[poolid];
        Participant storage participant = participants[msg.sender];
        Pool storage pool = pools[poolid];

        require(pool.isPoolIdUsed == true);
        require(voting.isVotingHappening == false);
        require(participant.tokens[poolid] > 0);

        voting.isVotingHappening = true;
        voting.propositon = PropositionValue;
        voting.proposer = msg.sender;

        voting.totalVotes = pool.numTokens;
        voting.yayVotes = participant.tokens[poolid];
        voting.nayVotes = 0;
        voting.hasVoted[msg.sender] = true;

        uint majority = voting.totalVotes/2;

        if(voting.yayVotes > majority) {
            finishVoting(true, poolid);
        }
    }

    function finishVoting(bool result, bytes32 poolid) private {
        Pool storage pool = pools[poolid];
        Voting storage voting = votings[poolid];

        if(result == true) {
            pool.minMarge = voting.propositon;
        }

        delete votings[poolid];
    }

    function cancelVoting(bytes32 poolid) public {
        Voting storage voting = votings[poolid];

        require(voting.isVotingHappening == true);
        require(voting.proposer == msg.sender);

        delete votings[poolid];

    }
    
    //função que registra os votos de um participante para decidir a nova margem mínima de um token.
    //recebe o id da pool e o valor dos votos do participante.
    //verifica se o participante já votou previamente e se ele tem tokens dessa pool.
    //marca o participante como já tendo votado e registra os votos dele para a média geral.
    function castVote(bytes32 poolid, bool vote) public {
        Voting storage voting = votings[poolid];
        Participant storage participant = participants[msg.sender];

        require(voting.isVotingHappening == true);
        require(participant.tokens[poolid] > 0);
        require(voting.hasVoted[msg.sender] == false);

        voting.hasVoted[msg.sender] = true;
        uint majority = voting.totalVotes/2;

        if (vote == true) {
            voting.yayVotes += participant.tokens[poolid];
            if(voting.yayVotes > majority) {
                finishVoting(true, poolid);
            }
        }
        else {
            voting.nayVotes += participant.tokens[poolid];
            if(voting.nayVotes > majority) {
                finishVoting(false, poolid);
            }
         }

    }

    //funcao para mudar a fee do contrato.
    function changeFee(uint newFee) public onlyOwner {
        ownerFee = newFee;
    }
    
    //funcao que muda quem é o dono do contrato, apenas o dono pode chamar,
    //passando o endereço do novo dono.
    function passOwnership(address newOwner) public onlyOwner {
        owner = newOwner;
    }

    //funcao que pode ser chamada apenas pelo dono que recolhe todo o
    //dinheiro armazenado no contrato através das taxas.
    function ownerWithdraw() public onlyOwner {
        msg.sender.transfer(totalFees);
    }

    //funcao que recebe o id de um fundo e retorna o fundo que tem o mesmo id, usada para razoes de teste
    function getPoolId(bytes32 poolid) public view returns (bytes32) {
        return pools[poolid].poolId;
    }

    //funcao retorna o valor minimo atual de venda das cotas de um fundo.
    function getMinMargin(bytes32 poolid) public view returns (uint) {
        return pools[poolid].minMarge;
    }

    //funcao que recebe um id de um fundo e retorna quantos tokens a pessoa que chamou a funcao tem naquele fundo.
    function getUserTokens(bytes32 poolid) public view returns (uint) {
        return participants[msg.sender].tokens[poolid];
    }

    //funcao que recebe um id de uma auction e retorna o id da mesma, usada para testes.
    function getAuctionId(uint auctionId) public view returns (uint) {
        return auctions[auctionId].id;
    }

    //funcao que recebe o id de uma auction e retorna o valor do preco do token dessa auction.
    function getAuctionValue(uint auctionId) public view returns (uint) {
        return auctions[auctionId].value;
    }

    //funcao que recebe o id de uma auction e retorna a quantidade de tokens dessa auction.
    function getAuctionAmount(uint auctionId) public view returns (uint) {
        return auctions[auctionId].amount;
    }

    //funcao que retorna o valor atual da fee do contrato.
    function getFee() public view returns (uint) {
        return ownerFee;
    }

    //retorna o numero de votos de um fundo.
    function getCurrentVotes(bytes32 poolid) public view returns (uint) {
        return votings[poolid].totalVotes;
    }

    //retorna se uma votação está ocorrendo naquele fundo.
    function getIsVoting(bytes32 poolid) public view returns (bool) {
        return votings[poolid].isVotingHappening;
    }

    //retorna a quantidade de votos a favor de uma votação de um fundo.
    function getCurrentYayVotes(bytes32 poolid) public view returns (uint) {
        return votings[poolid].yayVotes;
    }

    //retorna a quantidade de votos contrários uma votação de um fundo.
    function getCurrentNayVotes(bytes32 poolid) public view returns (uint) {
        return votings[poolid].nayVotes;
    }

    //retorna se o usuário já votou naquela votação.
    function getUserHasVoted(bytes32 poolid) public view returns (bool) {
        return votings[poolid].hasVoted[msg.sender];
    }

}
