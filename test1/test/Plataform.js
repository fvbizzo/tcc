const Plataform = artifacts.require('./Plataform.sol')


contract(Plataform, function(accounts) {

    let plataform;

    

    beforeEach('setup contract for each test', async function (){
        plataform = await Plataform.new();
    })
    
    it ('should get the right pool id', async function() {
        await plataform.newPool(10, web3.utils.asciiToHex('pool1'), 30, 20, {from: accounts[0]});

        const _poolid = web3.utils.toUtf8(await plataform.getPoolId(web3.utils.asciiToHex('pool1')));

        assert.equal(_poolid.valueOf(), 'pool1', "pool1 wasnt the pool id.");
    })

    it ('should create the first auction together with the pool creation', async function() {
        await plataform.newPool(10, web3.utils.asciiToHex('pool1'), 30, 20, {from: accounts[0]});

        const auctionid = await plataform.getAuctionId(0);

        assert.equal(auctionid, 0, "the auction id was different from 0.");
    })

    it ('should separete the amount of tokens between owner and auction during pool creation correctly', async function() {
        await plataform.newPool(10, web3.utils.asciiToHex('pool1'), 30, 20, {from: accounts[0]});

        const ownerAmount = await plataform.getUserTokens(web3.utils.asciiToHex('pool1'), {from: accounts[0]});
        const auctionAmount = await plataform.getAuctionAmount(0);

        assert.equal(auctionAmount, 20, "the auction amount was different from 20.");
        assert.equal(ownerAmount, 10, "the owner amount was different from 10")
    })

    it ('should get the right auction id', async function() {
        await plataform.newPool(10, web3.utils.asciiToHex('pool1'), 30, 20, {from:accounts[0]});
        await plataform.newAuction(5, web3.utils.asciiToHex('pool1'),10, {from:accounts[0]});

        const id = await plataform.getAuctionId(1);

        assert.equal(id, 1, "o id da Auction está errado");
    })

    it ('should get the right auction amount', async function() {
        await plataform.newPool(10, web3.utils.asciiToHex('pool1'), 30, 20, {from:accounts[0]});
        await plataform.newAuction(5, web3.utils.asciiToHex('pool1'),10, {from:accounts[0]});

        const amount = await plataform.getAuctionAmount(1);

        assert.equal(amount.valueOf(), 5, "the amount of tokens was not 5.")
    })

    it ('should get the right auction value', async function() {
        await plataform.newPool(10, web3.utils.asciiToHex('pool1'), 30, 20, {from:accounts[0]});
        await plataform.newAuction(5, web3.utils.asciiToHex('pool1'),10, {from:accounts[0]});

        const value = await plataform.getAuctionAmount(1);

        assert.equal(value.valueOf(), 5, "the amount of tokens was not 5.")
    })

    it ('should transfer the tokens', async function() {
        await plataform.newPool(10, web3.utils.asciiToHex('pool1'), 30, 10, {from:accounts[0]});
        await plataform.transferTokens(10, web3.utils.asciiToHex('pool1'), accounts[1], {from:accounts[0]});

        const rest = await plataform.getUserTokens(web3.utils.asciiToHex('pool1'), {from:accounts[0]});
        const received = await plataform.getUserTokens(web3.utils.asciiToHex('pool1'), {from:accounts[1]});

        assert.equal(rest.valueOf(), 10, "quantidade de tokens restantes com o dono esta errada");
        assert.equal(received.valueOf(), 10, "quantidade de tokens recebida esta errada");
    })

    it ('should cancel an auction', async function() {
        await plataform.newPool(10, web3.utils.asciiToHex('pool1'), 30, 10, {from:accounts[0]});
        await plataform.newAuction(5, web3.utils.asciiToHex('pool1'),10, {from:accounts[0]});

        await plataform.cancelAuction(1, {from:accounts[0]});

        try {
            const id = await plataform.getAuctionId(1);
            
            assert.equal(id, 1, "wrong auction id");
        } catch (error) {

        }


    })

    it ('should buy the auction', async function() {
        await plataform.newPool(10, web3.utils.asciiToHex('pool1'), 30, 10, {from:accounts[0]});
        await plataform.newAuction(5, web3.utils.asciiToHex('pool1'),10, {from:accounts[0]});

        await plataform.buyTokens(5, 1, {from:accounts[1], value: 2e+18});

        const tokens = await plataform.getUserTokens(web3.utils.asciiToHex('pool1'), {from:accounts[1]});
        const rest = await plataform.getAuctionAmount(1);

        assert.equal(tokens, 5, "numero de tokens esta incorreto.");
        assert.equal(rest, 0, "auction nao ficou vazia");


    })

    it ('should create a voting', async function() {
        await plataform.newPool(10, web3.utils.asciiToHex('pool1'), 30, 25, {from:accounts[0]});

        await plataform.newVoting(web3.utils.asciiToHex('pool1'), 75, {from:accounts[0]});

        const voting = await plataform.getIsVoting(web3.utils.asciiToHex('pool1'));

        assert.equal(voting, true, "a votação não foi criada.");


    })

    it ('should change the margin by voting', async function() {
        await plataform.newPool(10, web3.utils.asciiToHex('pool1'), 30, 10, {from:accounts[0]});

        await plataform.newVoting(web3.utils.asciiToHex('pool1'), 75, {from:accounts[0]});

        const novo_valor = await plataform.getMinMargin(web3.utils.asciiToHex('pool1'));

        assert.equal(novo_valor, 75, "a margem não foi mudada corretamente.");


    })
    it ('should cast a vote', async function() {
        await plataform.newPool(10, web3.utils.asciiToHex('pool1'), 100, 90, {from:accounts[0]});

        await plataform.buyTokens(10, 0, {from:accounts[1], value: 2e+18});
        await plataform.buyTokens(15, 0, {from:accounts[2], value: 2e+18});
        await plataform.buyTokens(10, 0, {from:accounts[3], value: 2e+18});

        await plataform.newVoting(web3.utils.asciiToHex('pool1'), 35, {from:accounts[2]});

        await plataform.castVote(web3.utils.asciiToHex('pool1'), false, {from:accounts[1]});


        const voting = await plataform.getIsVoting(web3.utils.asciiToHex('pool1'));
        const acc1_voted = await plataform.getUserHasVoted(web3.utils.asciiToHex('pool1'), {from:accounts[1]});
        const nay_votes = await plataform.getCurrentNayVotes(web3.utils.asciiToHex('pool1'));
        const yay_votes = await plataform.getCurrentYayVotes(web3.utils.asciiToHex('pool1'));

        assert.equal(voting, true, "a votação não foi criada.");
        assert.equal(acc1_voted, true, "a conta 1 não votou.");
        assert.equal(nay_votes, 10, "número de votos contrários está errado.");
        assert.equal(yay_votes, 15, "número de votos favoráveis está errado.");

    })



})
